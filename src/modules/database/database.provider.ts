import { DataSource } from "typeorm"

export const databaseProviders = [
    {
        provide: process.env.MYSQL_DATABASE,
        useFactory: async () =>{
            const dataSource = new DataSource({
                type: `mysql`,
                host: process.env.MYSQL_HOST,
                port: Number(process.env.MYSQL_PORT),
                username: process.env.MYSQL_USER,
                password: process.env.MYSQL_PASSWORD,
                database: process.env.MYSQL_DATABASE,
                entities: [
                    __dirname + '/../**/*.entity{.ts,.js}',
                ],
                synchronize: true,
                logging:'all'
              });
        
            return dataSource.initialize();

        }
    }
    
]