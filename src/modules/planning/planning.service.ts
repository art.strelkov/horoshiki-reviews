import { Inject, Injectable } from "@nestjs/common";
import { Cron, CronExpression } from "@nestjs/schedule";
import { DataSource } from "typeorm";
import { formatTypes } from "../telegram/telegram.constants";
import { TelegramBot } from "../telegram/telegram.service";
import { Planning, status } from "./planning.entity";

@Injectable()
export class PlanningService {
    constructor(
        @Inject(process.env.MYSQL_DATABASE)
        private dataSource: DataSource,
        private telegramBot: TelegramBot
    ) {
    }

    @Cron(CronExpression.EVERY_10_MINUTES)
    async executePlans() {
        const plans = await this.dataSource.manager.findBy(Planning, { status: status.PLANNED })
        for (const plan of plans) {
            const result = await this.telegramBot.sendMessage(`Спасибо за заказ. Оцените качество нашей работы по https://new.horoshiki.ru/reviews?entity=${plan.objectId}`, plan.phone, formatTypes.review, 5);
            if (result.status === 200) {
                plan.status = status.FULFILLED;
            }
        }
        await this.dataSource.manager.update(Planning, plans.filter((p) => p.status === status.FULFILLED).map((p) => ({ id: p.id })), { status: status.FULFILLED })
    }
}