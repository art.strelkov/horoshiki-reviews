import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

export enum status {
    PLANNED = 'planned',
    CANCELED = 'canceled',
    FULFILLED = 'fulfilled'
}

@Entity()
export class Planning {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    objectId: number;

    @Column()
    phone: string;

    @Column()
    date: Date;

    @Column({
        type: 'enum',
        enum: status,
        default: status.PLANNED
    })
    status: status
}
