import { Module } from "@nestjs/common";
import { Telegram } from "../telegram/telegram.module";
import { PlanningService } from "./planning.service";

@Module({
    imports: [Telegram],
    providers: [PlanningService],
    exports: []
})
export class PlanningModule { }