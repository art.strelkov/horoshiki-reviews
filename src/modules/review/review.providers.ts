import { DataSource, EntityManager } from 'typeorm';
import { ReviewDto } from './dto/review-dto';
import { ReviewObject } from './review-object.entity';
import { Review } from './review.entity';

export const reviewProviders = [
  {
    provide: process.env.REVIEW,
    useFactory: (dataSource: DataSource) => {
      return dataSource.getRepository(Review).extend({
        async getRating(idEntity: number): Promise<any> {
          return this.createQueryBuilder('R')
            .innerJoinAndSelect(
              ReviewObject,
              "RO",
              "R.id = RO.idReview AND RO.idObject = :idEntity",
              { idEntity: idEntity })
            .select("AVG(`R`.`rating`)", "rating")
            .groupBy("RO.idObject")
            .getRawOne()
        },
        async insertReview(review: ReviewDto) {
          return await this.manager.transaction(
            async (transactionManager: EntityManager) => {
              const { idEntity, ...reviewBody } = review;
              const insertResult = await transactionManager.insert(
                Review,
                reviewBody,
              );
              await transactionManager.insert(ReviewObject, {
                idReview: insertResult.identifiers[0].id,
                idObject: idEntity,
              });

              return { id: insertResult.identifiers[0].id, ...reviewBody };
            },
          );
        },
        async getReviews(idEntity: number): Promise<Review[]> {
          return this.createQueryBuilder('R')
            .innerJoin(
              ReviewObject,
              'RO',
              'RO.idReview = R.id AND RO.idObject = :idObject',
              { idObject: idEntity }
            )
            .select('R.*')
            .getRawMany()
        }
      });
    },
    inject: [process.env.MYSQL_DATABASE],
  },
  {
    provide: process.env.REVIEW_TO_OBJECT,
    useFactory: (dataSource: DataSource) => {
      dataSource.getRepository(ReviewObject);
    },
    inject: [process.env.MYSQL_DATABASE],
  },
];
