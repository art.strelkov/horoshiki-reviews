import { Module } from "@nestjs/common";
import { ClientModule } from "../client/client.module";
import { Telegram } from "../telegram/telegram.module";
import { ReviewController } from "./review.controller";
import { reviewProviders } from "./review.providers";
import { ReviewService } from "./review.service";

@Module(
    {
        imports: [
            ClientModule,
            Telegram
        ],
        providers: [
            ...reviewProviders,
            ReviewService
        ],
        exports: [...reviewProviders],
        controllers: [ReviewController]
    }
)
export class ReviewModule { }