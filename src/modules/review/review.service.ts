import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { formatTypes, } from '../telegram/telegram.constants';
import { TelegramBot } from '../telegram/telegram.service';
import { ReviewDto } from './dto/review-dto';
import { ReviewRepository } from './review.repository';

@Injectable()
export class ReviewService {
  constructor(
    @Inject(process.env.REVIEW)
    private reviewRepo: ReviewRepository,
    private tgBot: TelegramBot
  ) { }

  async createReview(review: ReviewDto) {
    const createdReview = await this.reviewRepo.insertReview(review);
    await this.tgBot.sendMessage(createdReview.text, `+79039272499`, formatTypes.review, review.rating);
    return createdReview;
  }

  async getReviews(idEntity: number) {
    const reviews = await this.reviewRepo.getReviews(idEntity);
    return reviews
  }

  async getRating(idEntity: number) {
    const rating = await this.reviewRepo.getRating(idEntity);
    if (rating === undefined) {
      throw new NotFoundException('Entity does not exist')
    }
    return rating;
  }
}
