export class ReviewDto{
    idEntity: number;

    rating: number;

    text: string;
}