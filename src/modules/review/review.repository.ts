import { Repository } from "typeorm";
import { ReviewDto } from "./dto/review-dto";
import { ReviewObject } from "./review-object.entity";
import { Review } from "./review.entity";

export class ReviewRepository extends Repository<Review>{
    async getRating(idEntity: number) {
        return this.createQueryBuilder('R')
            .innerJoinAndSelect(
                ReviewObject,
                "RO",
                "R.id = RO.idReview AND RO.idObject = :idEntity",
                { idEntity: idEntity })
            .select("AVG(R.rating)", "rating")
            .groupBy("RO.idObject")
            .getRawOne()
    }

    async insertReview(review: ReviewDto): Promise<Review> {
        return await this.manager.transaction(
            async (transactionManager) => {
                const { idEntity, ...reviewBody } = review;
                const insertResult = await transactionManager.insert(Review, reviewBody);
                await transactionManager.insert(ReviewObject, {
                    idReview: insertResult.identifiers[0].id,
                    idObject: idEntity
                })
                return { id: insertResult.identifiers[0].id, ...reviewBody };
            }
        )
    }

    async getReviews(idEntity: number): Promise<Review[]> {
        return this.createQueryBuilder('R')
            .innerJoin(
                ReviewObject,
                'RO',
                'RO.idReview = R.id AND RO.idObject = :idObject',
                { idObject: idEntity }
            )
            .select('R.*')
            .getRawMany()
    }
}