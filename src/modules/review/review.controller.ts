import { Body, Controller, Get, Param, Post } from "@nestjs/common";
import { ReviewDto } from "./dto/review-dto";
import { ReviewService } from "./review.service";

@Controller()
export class ReviewController {
    constructor(
        private reviewService: ReviewService
    ) { }

    @Get(':idEntity/reviews')
    async getReviewsForEntity(
        @Param('idEntity') idEntity: number
    ) {
        return this.reviewService.getReviews(idEntity)
    }

    @Get(':idEntity/rating')
    async getRatingForEntity(
        @Param('idEntity') idEntity: number
    ) {
        return this.reviewService.getRating(idEntity)
    }

    @Post()
    async makeReview(
        @Body() review: ReviewDto
    ) {
        return await this.reviewService.createReview(review);
    }
}
