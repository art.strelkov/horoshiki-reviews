import { Column, Entity, PrimaryColumn } from "typeorm";


@Entity()
export class ReviewObject {


    @PrimaryColumn({
        type: 'uuid'
    })
    idReview: number;

    @PrimaryColumn()
    idObject: number;

}