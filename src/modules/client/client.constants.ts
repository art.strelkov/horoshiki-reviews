import { Product } from "../product/product.entity"
import { Client } from "./client.entity"

export const clientConstants = {
    apiKey: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuaG9yb3NoaWtpLnJ1XC9hcGlcL2F1dGgiLCJpYXQiOjE2NTQ5MjM3NTYsImV4cCI6MTY1NTA0MTQ3OSwibmJmIjoxNjU0OTY5NDc5LCJqdGkiOiJoMmJ2RU5nU1pPb0djblhEIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.xkU-5pF_Uq9fXB8JGfdL85qwU3jha2ATPCM3bA3cGD4`
}

export type operatorChannelEvent = {
    event: string,
    data: operatorChannelData,
    socket?: any
}

type operatorChannelData = {
    order: order,
    socket?: any
}

export type order = {
    brand: brand,
    id: number,
    uid: string,
    id_1c?: string,
    status: number,
    created_date: string,
    created_time: string,
    person_amount: number,
    total_price: number,
    notes?: string[],
    receiving: receiving,
    execution: execution,
    payment: payment,
    cart: cart,
    bonuses: bonuses,
    client: eventClient
}

type brand = {
    alias: string,
    name: string
}

type receiving = {
    type: string,
    pickup_addres: receivingAddress
}

type receivingAddress = {
    id: number,
    address: string
}

type execution = {
    scheduled_delivery_at_timestamp: number,
    type: string,
    duration?: number,
    date?: Date,
    time?: string
}

type payment = {
    type: string,
    note?: string
}

type cart = {
    items: item[],
    additions: item[],
    free: item[],
}

type item = Product & {
    name: string,
    quantity: number,
    price: number,
    has_modifications: boolean,
    generated_id: string,
    is_unavailable: boolean,
    is_promotion?: boolean,
    is_promotional?: boolean,
    categories: number[]
}


type bonuses = {
    total: number,
    paid: number,
    received: number,
}

type birth = {
    day: string,
    month: string
}

type eventClient = Client & {
    birth: birth
}