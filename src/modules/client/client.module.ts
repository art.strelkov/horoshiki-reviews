import { Module } from "@nestjs/common";
import { DatabaseModule } from "../database/database.module";
import { clientProviders } from "./client.providers";

@Module(
    {
        imports: [DatabaseModule],
        providers: [
            ...clientProviders
        ],
        exports: [
            ...clientProviders
        ]
    }
)
export class ClientModule { }