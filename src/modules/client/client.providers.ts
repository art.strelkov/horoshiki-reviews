import { DataSource } from "typeorm";
import { clientConstants } from "./client.constants";
import { Client } from "./client.entity";

export const clientProviders = [
    {
        provide: process.env.CLIENT,
        useFactory: (dataSource: DataSource) => { dataSource.getRepository(Client) },
        inject: [process.env.MYSQL_DATABASE]
    },
    {
        provide: process.env.CLIENTAPIKEY,
        useFactory: () => clientConstants.apiKey
    }
]