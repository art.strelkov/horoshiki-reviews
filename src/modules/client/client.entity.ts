import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Client {
    @PrimaryColumn({
        type: 'varchar',
        length: 20
    })
    phone: string;

    @Column({ type: 'varchar', length: 30, nullable: true })
    name?: string;
}