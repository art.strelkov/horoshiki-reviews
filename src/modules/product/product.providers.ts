import { DataSource } from "typeorm";
import { Product } from "./product.entity";

export const productProdvider = [
    {
        provide: process.env.PRODUCT,
        useFactory: (dataSource: DataSource) => dataSource.getRepository(Product),
        inject: [process.env.MYQSL_DATABASE]
    }
]