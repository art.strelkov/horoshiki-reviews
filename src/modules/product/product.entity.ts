import { Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Product {
    @PrimaryColumn({
        type: 'int'
    })
    id: number;
}