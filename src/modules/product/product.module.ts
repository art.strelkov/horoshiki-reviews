import { Module } from "@nestjs/common";
import { productProdvider } from "./product.providers";

@Module({
    imports: [],
    providers: [...productProdvider],
    exports: [...productProdvider],
})
export class ProductModule { }