export type operatorChannelEvent = {
    event: string,
    data: operatorChannelData,
    socket?: any
}

type operatorChannelData = {
    order: order,
    socket?: any
}

export type order = {
    brand: brand,
    id: number,
    uid: string,
    id_1c?: string,
    status: number,
    created_date: string,
    created_time: string,
    person_amount: number,
    total_price: number,
    notes?: string[],
    receiving: receiving,
    execution: execution,
    payment: payment,
    cart: cart,
    bonuses: bonuses,
    client: eventClient
}

type brand = {
    alias: string,
    name: string
}

type receiving = {
    type: string,
    pickup_addres: receivingAddress
}

type receivingAddress = {
    id: number,
    address: string
}

type execution = {
    scheduled_delivery_at_timestamp: number,
    type: string,
    duration?: number,
    date?: Date,
    time?: string
}

type payment = {
    type: string,
    note?: string
}

type cart = {
    items: item[],
    additions: item[],
    free: item[],
}

type item = Product & {
    name: string,
    quantity: number,
    price: number,
    has_modifications: boolean,
    generated_id: string,
    is_unavailable: boolean,
    is_promotion?: boolean,
    is_promotional?: boolean,
    categories: number[]
}


type bonuses = {
    total: number,
    paid: number,
    received: number,
}

type birth = {
    day: string,
    month: string
}

type eventClient = Client & birth