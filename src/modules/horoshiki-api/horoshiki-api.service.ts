import { Inject, Injectable, OnModuleInit } from "@nestjs/common";
import { operatorChannelEvent, order } from "./horoshiki-api";
import * as io from 'socket.io-client';
import { DataSource } from "typeorm";
import { Client } from "../client/client.entity";
import { Order } from "../order/order.entity";
import { Product } from "../product/product.entity";
import { Planning, status } from "../planning/planning.entity";
import { TelegramBot } from "../telegram/telegram.service";
import { formatTypes } from "../telegram/telegram.constants";

@Injectable()
export class HoroshikiApiService implements OnModuleInit {
    constructor(
        @Inject(process.env.MYSQL_DATABASE)
        private dataSource: DataSource,
        private telegramBot: TelegramBot
    ) {
    }
    onModuleInit() {
        const horoshikiApi = io('https://api.dev.horoshiki.ru:5000', { rejectUnauthorized: false })
        horoshikiApi.on('operator_channel', (data: string) => {
            const event: operatorChannelEvent = JSON.parse(data);
            const [entity, action] = event.event.split('.');
            if (this[entity]) {
                const entityProceedService = this[entity]();
                entityProceedService[action] && entityProceedService[action](event.data.order);
            }
        })


    }

    order = () => {
        return {
            created: async (order: order) => {
                const manager = this.dataSource.manager;
                await manager.transaction(async (transactionManager) => {
                    const clientRepository = transactionManager.getRepository(Client);
                    const orderRepository = transactionManager.getRepository(Order);
                    const productRepository = transactionManager.getRepository(Product);

                    let productIds = [];
                    for (const category in order.cart) {
                        productIds = productIds.concat(order.cart[category])
                    }

                    const { birth, ...client } = order.client;
                    productIds = productIds.map((p) => ({ id: p.id }));
                    await productRepository.upsert(productIds, ['id'])
                    await clientRepository.upsert(client, ['phone']);
                    await orderRepository.insert({ id: order.id, client: client.phone })
                    await this.telegramBot.sendMessage(`Добавил информацию о заказе ${order.id}.`, order.client.phone, formatTypes.review, 5)
                })
            },

            changed: async (order: order) => {
                const planningRepository = this.dataSource.getRepository(Planning);
                switch (order.status) {
                    case 2: {
                        const result = await planningRepository.insert({ objectId: order.id, phone: order.client.phone, date: new Date(), status: status.PLANNED })
                        await this.telegramBot.sendMessage(`Добавил информацию о заказе ${order.id}. Идентификатор планирования: ${result.identifiers[0]}`, order.client.phone, formatTypes.review, 5)
                        break;
                    }
                    case 3: {
                        const result = await planningRepository.update({ objectId: order.id, phone: order.client.phone }, { status: status.CANCELED })
                        await this.telegramBot.sendMessage(`Отменил планирование для зазказа ${order.id}`, order.client.phone, formatTypes.report, 2)
                        break;
                    }

                }
            }
        }
    }
}