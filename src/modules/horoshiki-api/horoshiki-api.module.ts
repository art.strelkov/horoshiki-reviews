import { Module } from "@nestjs/common";
import { Telegram } from "../telegram/telegram.module";
import { HoroshikiApiService } from "./horoshiki-api.service";

@Module(
    {
        imports: [Telegram],
        providers: [HoroshikiApiService],

    }
)
export class HoroshikiApi { }