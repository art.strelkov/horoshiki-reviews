import { Inject, Injectable } from "@nestjs/common";
import axios, { AxiosRequestConfig } from "axios";
import { formatTypes, TelegramConstants } from "./telegram.constants";

@Injectable()
export class TelegramBot {
    constructor(
        @Inject(process.env.TELEGRAMCONST)
        private tgConstants: typeof TelegramConstants
    ) { }


    async sendMessage(text: string, clientPhone: string | number, type: formatTypes, rating?: number) {
        const params = this.tgConstants.getMessage(this.tgConstants.reviewsChatId, text, type, clientPhone, rating);
        // const bot = await axios({
        //     method: 'GET',
        //     url: `${this.tgConstants.apiLink}/${this.tgConstants.token}/getChat`,
        //     params: {
        //         chat_id: `${this.tgConstants.reviewsChatId}`
        //     }
        // })
        // console.log(bot)
        let message
        let config: AxiosRequestConfig = {
            method: `POST`,
            url: `${this.tgConstants.apiLink}/${this.tgConstants.token}/sendMessage`,
            headers: { Charset: `UTF-8` },
            data: { ...params }
        }
        try {
            message = await axios(config)
        } catch (err) {
            console.log(err)
        }
        return message;
    }

}