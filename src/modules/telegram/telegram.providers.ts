import { TelegramConstants } from "./telegram.constants";

export const telegramProviders = [
    {
        provide: process.env.TELEGRAMCONST,
        useFactory: () => TelegramConstants
    }
]