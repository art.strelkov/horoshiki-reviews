export const TelegramConstants = {
    // token: `bot5438009248:AAGxF-bksczgjRjAS-xkQo2Viz6ovCUdskw`,
    token: `bot5311981265:AAF5TiMGJccgiW-pZoWAIZkoZwNmp7p-QUY`,
    apiLink: `https://api.telegram.org`,
    // reviewsChatId: `-1001737135574`,
    reviewsChatId: `-1001474439747`,
    getMessage: (chat_id: number | string, reviewText: string, type: formatTypes, clientPhone: number | string, rating?: number) => {
        const text = `${typeSymbol[type]} <b>${messageType[type]}</b>\n<b>Номер клиента </b>: ${clientPhone}\n${rating ? `<b>Рейтинг </b>: ` + rating + ` \n\n` : ``}<b>Отзыв:\n</b>${reviewText.split('.').join('\n')}
`


        const params = {
            chat_id,
            text,
            parse_mode: 'HTML'
        }
        return params
    }
}

export enum formatTypes {
    review = 'review',
    report = 'report'
}

export enum messageType {
    review = `Новый отзыв`,
    report = `Новая жалоба`
}

export enum typeSymbol {
    review = `\u2705`,
    report = `\u2757`
}