import { Module } from "@nestjs/common";
import { telegramProviders } from "./telegram.providers";
import { TelegramBot } from "./telegram.service";

@Module({
    providers: [
        ...telegramProviders,
        TelegramBot
    ],
    exports: [TelegramBot],
})
export class Telegram {

}