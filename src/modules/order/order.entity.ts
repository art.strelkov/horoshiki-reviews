import { Entity, ManyToOne, PrimaryColumn } from "typeorm";
import { Client } from "../client/client.entity";

@Entity()
export class Order {
    @PrimaryColumn()
    id: number;

    @ManyToOne(() => Client)
    client: string;

}