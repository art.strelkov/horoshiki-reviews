import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { ClientModule } from './modules/client/client.module';
import { DatabaseModule } from './modules/database/database.module';
import { HoroshikiApi } from './modules/horoshiki-api/horoshiki-api.module';
import { OrderModule } from './modules/order/order.module';
import { PlanningModule } from './modules/planning/planning.module';
import { ReviewModule } from './modules/review/review.module';
import { Telegram } from './modules/telegram/telegram.module';

@Module({
  imports: [
    DatabaseModule,
    ReviewModule,
    ClientModule,
    Telegram,
    HoroshikiApi,
    OrderModule,
    PlanningModule,
    ScheduleModule.forRoot()
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
